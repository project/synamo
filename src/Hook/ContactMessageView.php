<?php

namespace Drupal\synamo\Hook;

use Drupal\Core\Entity\EntityInterface;

/**
 * Entity View.
 */
class ContactMessageView {

  /**
   * Hook.
   */
  public static function hook(&$build, EntityInterface $entity, $view_mode) {
    if ($view_mode == 'full') {
      \Drupal::messenger()->addWarning(__CLASS__ . " view-full");
      $id = $entity->id();
      \Drupal::service('amo.amo_connector')->init($id, FALSE);
    }
  }

}
