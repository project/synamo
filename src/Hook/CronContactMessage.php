<?php

namespace Drupal\synamo\Hook;

/**
 * Hook Cron.
 */
class CronContactMessage {

  /**
   * Hook.
   */
  public static function hook() {
    \Drupal::service('amo.amo_connector')->queueProcess();
  }

}
