<?php

namespace Drupal\synamo\Hook;

use Drupal\Core\Entity\EntityInterface;

/**
 * Entity View.
 */
class WebformMessageView {

  /**
   * Hook.
   */
  public static function hook(&$build, EntityInterface $entity, $view_mode) {
    if ($view_mode == 'table') {
      $matomo = $entity->matomo->value;
      SyncloudQueuePreprocessWebform::hook($entity, $matomo, TRUE);
      // \Drupal::service('amo.amo_connector')->init($id, TRUE);
    }
  }

}
