<?php

namespace Drupal\synamo\Hook;

/**
 * Hook Cron.
 */
class CronCompleteOrderToAmo {

  /**
   * Hook.
   */
  public static function hook() {
    \Drupal::service('amo.amo_connector')->processQueueCompleteOrderToAmo();
  }

}
