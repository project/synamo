<?php

namespace Drupal\synamo\Hook;

use Drupal\Core\Entity\EntityInterface;

/**
 * Contact Message Presave.
 */
class ContactMessagePresave {

  /**
   * Hook.
   */
  public static function hook(EntityInterface $entity, $init = FALSE) {
    $extra = $entity->extra->value;
    if (empty($extra)) {
      // Go to Drupal\synamo\Hook\EntityBaseFieldInfo.
    }
  }

}
