<?php

namespace Drupal\synamo\Hook;

use Drupal\Core\Entity\EntityInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * Entity Insert.
 */
class SyncloudQueuePreprocessWebform {

  /**
   * Hook.
   */
  public static function hook(EntityInterface $entity, $init = FALSE) {
    $webform_context_categories = [
      'SEO',
      'Контекст',
    ];
    /** @var \Drupal\webform\WebformSubmissionInterface $entity */
    $webform = $entity->getWebform();
    $id = $webform->id();
    $categories_array = $webform->get('categories');
    /** @var \Drupal\webform\WebformInterface $webform */
    $uri = $entity->uri->value;
    $body = self::messageToBody($entity, $webform);
    // $matomo = $entity->matomo->value;
    // if ($matomo) {
    //   $connector = \Drupal::service('amo.amo_connector');
    //   $source = $connector->matomoToSource($matomo);
    // }
    $otdel = '9893595';
    // Пеунова Катя
    // $resposible_user_id = '6592353';.
    // Руководители (админ)
    $resposible_user_id = '6255819';

    $pipeline = '7038';
    $service = '1476656';
    if (in_array('Other', $categories_array)) {
      $otdel = '41165337';
      $resposible_user_id = '6218907';
      $pipeline = '4440975';
      // '1462110';
      $service = '1476674';
    }
    if (in_array('WEB', $categories_array) || $id == 'site_price') {
      $service = '1476674';
    }
    if ($id == 'recommendation') {
      $resposible_user_id = '6255819';
      $pipeline = '3256176';
    }
    $custom_fields = [
      // 'matomo' => $matomo,
      // 'ga_client_id' => $extra['google'],
      // 'calltouch' => $extra['calltouch'],
      // 'yandex' => $extra['yandex'],
      // 'city' => $message->city->value,
      'site' => $uri,
      'body' => $body,
      'pipeline_id' => $pipeline,
      'status_id' => $otdel,
      'responsible_user_id' => $resposible_user_id,
      'service' => [$service],
      'handling' => ['1468866'],
      'utm' => mb_substr($source['utm'], 0, 255) ?? NULL,
      'channel' => [$source['channel_id'] ?? NULL],
    ];
    $lead = [
      'name' => $webform->get('title'),
      'pipeline_id' => $custom_fields['pipeline_id'],
      'status_id' => $custom_fields['status_id'],
      'responsible_user_id' => $custom_fields['responsible_user_id'],
      'tags' => 'c сайтa',
      'custom_fields' => self::mapCustomFields($custom_fields),
    ];
    // \Drupal::messenger()->addWarning($body);
    $amo = \Drupal::service('amo.api');
    if (!$init) {
      $amo_lead = $amo->addLead($lead);
      \Drupal::logger(__FUNCTION__ . __LINE__)->notice(
        '@j', ['@j' => json_encode($amo_lead)]
      );
      $phone = '';
      $name = '';
      foreach ($entity->getData() as $key => $value) {
        if (in_array($key, ['phone', 'telefon', 'hone', 'contact_phone'])) {
          $phone = $value;
        }
        elseif (in_array($key, ['vashe_imya', 'name', 'contact_name'])) {
          $name = $value;
        }
      }
      if (!($contact = $amo->checkContact($entity, TRUE, $phone))) {
        $contact = [
          'name' => $name ?? 'Посетитель сайта',
          'linked_leads_id' => $amo_lead,
          'custom_fields' => [
            'phone' => [
              'id' => '420388',
              'values' => [
                ['value' => $phone],
              ],
            ],
          ],
        ];
        \Drupal::service('amo.api')
          ->addContact($contact, $amo_lead);
      }
      else {
        if ($amo_lead && $contact) {
          $link_update = $amo->updateLeadLinks($amo_lead, $contact);
        }
      }
    }

  }

  /**
   * Init funciton.
   */
  private static function messageToBody(object $entity, object $webform): string {
    $fields = [];
    $body = '';
    foreach ($entity->getData() as $key => $value) {
      $element = $webform->getElement($key);
      $title = $element ? $element['#title'] : 'Нет имени поля...';
      $title = str_replace("/", "или", $title);
      if ($value) {
        if (is_array($value)) {
          $body .= "$title - ";
          $num_items = count($value);
          $i = 0;
          $separator = ' | ';
          foreach ($value as $item) {
            if (++$i === $num_items) {
              $separator = '';
            }
            $body .= $element['#options'][$item] . $separator;
          }
          $body = $body . "\n";
        }
        else {
          $body .= "$title - $value\n";
        }
      }
    }
    return $body;
  }

  /**
   * Map.
   */
  private static function mapCustomFields($custom_fields) {
    $fields = [];
    $map = [
      '525504' => 'body',
      '589816' => 'matomo',
      '589818' => 'ga_client_id',
      '587868' => 'site',
      '420390' => 'email',
      '420388' => 'phone',
      // '648028' => 'service',
      '723856' => 'service',
      '632884' => 'utm',
      '600935' => 'channel',
      '717786' => 'handling',
      '648312' => 'utm_content',
      '648160' => 'utm_medium',
      '648310' => 'utm_campaign',
      '648158' => 'utm_source',
      '648314' => 'utm_term',
      '650237' => 'utm_referrer',
    ];
    foreach ($map as $k => $v) {
      if (isset($custom_fields[$v])) {
        if ($v == 'email') {
          $values = [
            [
              'value' => $custom_fields[$v],
              'enum' => 'WORK',
            ],
          ];
        }
        else {
          $values = [['value' => $custom_fields[$v]]];
        }
        $fields[$v] = ['field_id' => $k, 'values' => $values];
      }
    }
    $fields = array_values($fields);
    return $fields;
  }

  /**
   * Hook.
   */
  private static function guzzlePushQueue() {
    $client = new Client([
      'base_uri' => 'https://www.synapse-studio.ru',
      'timeout'  => 0.1,
    ]);
    try {
      $response = $client->get('https://www.synapse-studio.ru/amo/queue');
      $message = $response->getStatusCode();
    }
    catch (ConnectException $e) {
      $message = 'ConnectException ' . $e->getMessage();
    }
    catch (RequestException $e) {
      $message = 'RequestException ' . $e->getMessage();
    }
    catch (ClientException $e) {
      $message = 'ClientException ' . $e->getMessage();
    }
    catch (GuzzleException $e) {
      $message = 'GuzzleException ' . $e->getMessage();
    }
    \Drupal::logger(__CLASS__)->notice("RequestException::$message");
    return $message;
  }

}
