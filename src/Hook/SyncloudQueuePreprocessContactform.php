<?php

namespace Drupal\synamo\Hook;

use Drupal\Core\Entity\EntityInterface;

/**
 * Entity Insert.
 */
class SyncloudQueuePreprocessContactform {

  /**
   * Hook.
   */
  public static function hook(EntityInterface $entity, $init = FALSE) {
    \Drupal::service('amo.amo_connector')->init($entity->id());
  }

}
