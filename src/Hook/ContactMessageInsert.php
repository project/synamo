<?php

namespace Drupal\synamo\Hook;

use Drupal\Core\Entity\EntityInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * Entity Insert.
 */
class ContactMessageInsert {

  /**
   * Hook.
   */
  public static function hook(EntityInterface $entity, $init = FALSE) {
    if ($entity->getEntityType()->id() == 'contact_message') {
      // Добавляем в очередь контактное сообщение.
      $id = $entity->id();
      \Drupal::service('amo.amo_connector')->init($id);
    }
  }

  /**
   * Hook.
   */
  private static function guzzlePushQueue() {
    $client = new Client([
      'base_uri' => 'https://www.synapse-studio.ru',
      'timeout'  => 1,
    ]);
    try {
      $response = $client->get('https://www.synapse-studio.ru/amo/queue');
      $message = $response->getStatusCode();
    }
    catch (ConnectException $e) {
      $message = 'ConnectException ' . $e->getMessage();
    }
    catch (RequestException $e) {
      $message = 'RequestException ' . $e->getMessage();
    }
    catch (ClientException $e) {
      $message = 'ClientException ' . $e->getMessage();
    }
    catch (GuzzleException $e) {
      $message = 'GuzzleException ' . $e->getMessage();
    }
    \Drupal::logger(__CLASS__)->notice("$message");
    return $message;
  }

}
