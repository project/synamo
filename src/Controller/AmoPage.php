<?php

namespace Drupal\synamo\Controller;

/**
 * @file
 * Contains \Drupal\synamo\Controller\amo.
 */

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller AppPage.
 */
class AmoPage extends ControllerBase {

  /**
   * Render Page.
   */
  public function page(Request $request) {
    $link = "";
    $text = $this->t('AmoCRM API');
    $amo = \Drupal::service('amo.api');
    $auth = $amo->oAuth2($request);
    if (!is_object($auth)) {
      return [
        'text' => ['#markup' => "<a href='$auth'>Пройдите авторизацию</a>"],
      ];
    }
    $users = $amo->getUsers();
    return [
      'text' => ['#markup' => "<p>{$text}</p>"],
    ];
  }

  /**
   * Render Page.
   */
  public function cron(Request $request) {
    $amo = \Drupal::service('amo.api');
    $amo->refreshToken();
    return [
      'text' => ['#markup' => "<p>Тест работы крона</p>"],
    ];
  }

  /**
   * Title.
   */
  public function getUserInfo() {
    return "Amo API";
  }

}
