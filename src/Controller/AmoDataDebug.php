<?php

namespace Drupal\synamo\Controller;

/**
 * @file
 * Contains \Drupal\synamo\Controller\AppPage.
 */

use AmoCRM\Client\AmoCRMApiClient;
use AmoCRM\Models\ContactModel;
use AmoCRM\Models\LeadModel;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\HttpFoundation\Request;

/**
 * Amo debug page.
 */
class AmoDataDebug extends ControllerBase {

  /**
   *
   */
  public function __construct() {
    $config = \Drupal::config('synamo.settings');
    $this->token = '';
    $this->client = NULL;
    $this->clientId = $config->get('id');
    $this->clientSecret = $config->get('key');
    $this->host = $config->get('amohost');
    $this->cookieDir = DRUPAL_ROOT . '/sites/default/private/amoCookie.txt';
    // ...
    if (!$this->host) {
      \Drupal::messenger()->addError('Адрес сервера не установлен');
      return FALSE;
    }
    $this->redirectUri = $config->get('ourhost') . '/amo';
    $this->config = $config = \Drupal::service('config.factory')->getEditable('amo.token');
    $this->client = new AmoCRMApiClient($this->clientId, $this->clientSecret, $this->redirectUri);
    $this->token = $this->getToken();
    $this->client->setAccessToken($this->token)
      ->setAccountBaseDomain($this->host);
  }

  /**
   * получить сделки.
   */
  public function getContacts() {
    $client = new AmoCRMApiClient($this->clientId, $this->clientSecret, $this->redirectUri);
    $token = $this->getToken();
    dsm($this->token);
    $client->setAccessToken($token)
      ->setAccountBaseDomain($this->host);
    $number = urlencode('+7(111) 111-11-11');
    dsm($number);
    $link = 'https://' . $this->host . '/api/v4/contacts?query=' . $number;
    // $link = 'https://' . $this->host . '/api/v4/leads/29336536';
    $headers = [
      'Content-Type: application/hal+json',
      'Authorization: Bearer ' . $this->token->getToken(),
    ];
    dsm($this->token->getToken());
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
    curl_setopt($curl, CURLOPT_HEADER, FALSE);
    curl_setopt($curl, CURLOPT_URL, $link);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_HEADER, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
    $out = curl_exec($curl);
    $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    $code = (int) $code;
    $errors = [
      301 => 'Moved permanently.',
      400 => 'Wrong structure of the array of transmitted data, or invalid identifiers of custom fields.',
      401 => 'Not Authorized. There is no account information on the server. You need to make a request to another server on the transmitted IP.',
      403 => 'The account is blocked, for repeatedly exceeding the number of requests per second.',
      404 => 'Not found.',
      500 => 'Internal server error.',
      502 => 'Bad gateway.',
      503 => 'Service unavailable.',
    ];

    if ($code < 200 || $code > 204) {
      die("Error $code. " . ($errors[$code] ?? 'Undefined error'));
    }
    $amolead = new LeadModel();
    dsm($amolead);
    $amocontact = new ContactModel();
    // $amocontact->getBy('id', 46949420);
    dsm($client->companies()->getOne(46960386));
    dsm($client->leads()->getOne(29299748));
    dsm($client->contacts()->getOne(46949420));
    $customLinks = $client->contacts()->getLinks($client->contacts()->getOne(46949420));
    dsm($customLinks->getBy('toEntityType', 'leads'));
    return Json::decode($out);
  }

  /**
   *
   */
  public function checkContact() {
    $number = str_replace(['(', ')', '-', ' '], '', '+7(111)111-11-11');
    $link = 'https://' . $this->host . '/api/v4/contacts?query=' . $number;
    $headers = [
      'Content-Type: application/hal+json',
      'Authorization: Bearer ' . $this->token,
    ];
    \Drupal::logger(__FUNCTION__ . __LINE__)->notice(
      '@j', ['@j' => json_encode($this->token)]
    );
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
    curl_setopt($curl, CURLOPT_HEADER, FALSE);
    curl_setopt($curl, CURLOPT_URL, $link);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_HEADER, FALSE);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
    $out = curl_exec($curl);
    $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    $code = (int) $code;
    $errors = [
      301 => 'Moved permanently.',
      400 => 'Wrong structure of the array of transmitted data, or invalid identifiers of custom fields.',
      401 => 'Not Authorized. There is no account information on the server. You need to make a request to another server on the transmitted IP.',
      403 => 'The account is blocked, for repeatedly exceeding the number of requests per second.',
      404 => 'Not found.',
      500 => 'Internal server error.',
      502 => 'Bad gateway.',
      503 => 'Service unavailable.',
    ];
    \Drupal::logger(__FUNCTION__ . __LINE__)->notice(
        '@j', ['@j' => json_encode($out)]
      );
    if ($code < 200 || $code > 204) {
      die("Error $code. " . ($errors[$code] ?? 'Undefined error'));
    }
    if (!$out) {
      return FALSE;
    }
    $result = Json::decode($out);
    $contacts = $result['_embedded']['contacts'];
    dsm($contacts);
    $contact = $this->client->contacts()->getOne($contacts[0]['id']);
    return $contact ?? FALSE;
  }

  /**
   * Token get.
   */
  private function getToken() {
    $token = $this->config->get('tok');
    return new AccessToken($token);
  }

  /**
   *
   */
  public function getData() {
    $contact = [
      'name' => 'Тест1',
      'linked_leads_id' => $amo_lead,
      'custom_fields' => [
        'phone' => [
          'id' => '420388',
          'values' => [
            ['value' => '+7(111) 111-11-11'],
          ],
        ],
      ],
    ];
    // \Drupal::service('amo.api')
    //   ->addContact($contact);
  }

  /**
   *
   */
  public function getContactByPhone($array) {

    // /**
    //  *
    //  */
    $result = array_filter($array['_embedded']['contacts'], function ($item) {
      if (isset($item['custom_fields_values'])) {
        $item_with_phone = array_filter($item['custom_fields_values'], function ($field) {
          return $field['field_id'] == 420388;
        });
        return count($item_with_phone);
      }
      return FALSE;
    });
    $result = array_map(function ($item) {
      $phone_field = array_filter($item['custom_fields_values'], function ($field) {
        return $field['field_id'] == 420388;
      });
      return [
        $item['id'],
        array_shift($phone_field)['values'][0]['value'],
      ];
    }, $result);
    dsm($result);
  }

  /**
   * Render Page.
   */
  public function page(Request $request) {
    $contacts = '';
    $contacts = $this->getContacts();
    $contacts2 = $this->checkContact();
    $data = [];
    $build = ['#markup' => json_encode([$contacts, $contacts2])];
    return $build;
  }

}
