<?php

namespace Drupal\synamo\Service;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\user\Entity\User;

/**
 * Class Connector.
 */
class AmoConnector {

  /**
   * Construct funciton.
   */
  public function __construct() {
    $this->storage = \Drupal::entityTypeManager()->getStorage('contact_message');
    $this->default_tags = 'с сайтa';
    $this->form_config = \Drupal::service('config.factory')->get('synamo.lead_settings');

    if ($this->form_config) {
      $this->pipeline = [
        'lead' => [
          'id' => $this->form_config->get('status-id'),
          'pipe' => $this->form_config->get('pipeline-id'),
        ],
      ];
    }
  }

  /**
   * Get order body.
   */
  private function getOrderItemLines(OrderInterface $order) : array {
    $lines = [];
    foreach ($order->getItems() as $item) {
      /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $variation */
      $variation = $item->getPurchasedEntity();
      if (!is_object($variation)) {
        continue;
      }

      /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
      $product = $variation->getProduct();
      if (!is_object($product)) {
        continue;
      }

      $unit_price = $this->formatPrice(
        $item->getUnitPrice()->getNumber()
      );
      $total_price = $this->formatPrice(
        $item->getTotalPrice()->getNumber()
      );
      $lines[] = sprintf(
        ' - %s (%d) цена %s %d шт. %s',
        $product->getTitle(), $product->id(), $unit_price, $item->getQuantity(), $total_price
      );
    }
    return $lines;
  }

  /**
   * Format price.
   */
  private function formatPrice(float $price) {
    return number_format($price, 0, ' ', ' ') . ' ₽';
  }

  /**
   * Get order body.
   */
  private function getOrderBillingProfileLines(OrderInterface $order) : array {
    $billing_profile = $order->getBillingProfile();
    if (empty($billing_profile)) {
      return [];
    }
    return [
      ' - Имя: ' . $billing_profile->field_customer_name->value,
      ' - Фамилия: ' . $billing_profile->field_customer_surname->value,
      ' - Телефон: ' . $billing_profile->field_customer_phone->value,
      ' - E-mail: ' . $billing_profile->field_customer_email->value,
      ' - Комментарий: ' . $billing_profile->field_customer_comment->value,
    ];
  }

  /**
   * Get order body.
   */
  private function getOrderBody(OrderInterface $order) : string {
    $total_price = $this->formatPrice(
      $order->getTotalPrice()->getNumber()
    );
    $lines = [
      'Ид заказа: ' . $order->id(),
      'Номер заказа: ' . $order->getOrderNumber(),
      'Информация о заказе:',
      implode("\n", $this->getOrderBillingProfileLines($order)),
      'Позиции заказа:',
      implode("\n", $this->getOrderItemLines($order)),
      'Итого: ' . $total_price,
    ];
    return implode("\n", $lines);
  }

  /**
   * Add complete order lead.
   */
  public function addCompleteOrderLead(OrderInterface $order, $debug = FALSE) {
    $body = $this->getOrderBody($order);
    $otdel = $this->pipeline['lead'];
    $custom_fields = [
      'body' => $body,
      'pipeline_id' => $otdel['pipe'],
      'status_id' => $otdel['id'],
    ];
    $lead = [
      'name' => "Заказ из корзины {$order->id()}",
      'pipeline_id' => $custom_fields['pipeline_id'],
      'status_id' => $custom_fields['status_id'],
      'tags' => $this->default_tags,
      'custom_fields' => $this->mapCustomFields($custom_fields),
    ];
    $amo = \Drupal::service('amo.api');
    if (!$debug) {
      $amo_lead = $amo->addLead($lead);
      if (!($contact = $amo->checkContact($order->getBillingProfile()))) {
        $contact = [
          'name' => $order->getBillingProfile()->field_customer_name->value ?? 'Посетитель сайта',
          'linked_leads_id' => $amo_lead,
          'custom_fields' => [
            'phone' => [
              'id' => $this->form_config->get('phone-id'),
              'values' => [
                ['value' => $order->getBillingProfile()->field_customer_phone->value ?? FALSE],
              ],
            ],
          ],
        ];
        \Drupal::service('amo.api')
          ->addContact($contact, $amo_lead);
      }
      else {
        $link_update = $amo->updateLeadLinks($amo_lead, $contact);
      }
    }
  }

  /**
   * Init function.
   */
  public function init(int $id, $debug = FALSE) {
    $message = $this->storage->load($id);
    $body = $this->messageToBody($message);
    $otdel = $this->pipeline['lead'];
    $lead_name = $message->getContactForm()->label();
    $custom_fields = [
      'body' => $body,
      'pipeline_id' => $otdel['pipe'],
      'status_id' => $otdel['id'],
    ];
    $lead = [
      'name' => $lead_name,
      'pipeline_id' => $custom_fields['pipeline_id'],
      'status_id' => $custom_fields['status_id'],
      'tags' => $this->default_tags,
      'custom_fields' => $this->mapCustomFields($custom_fields),
    ];
    $amo = \Drupal::service('amo.api');
    if (!$debug) {
      $amo_lead = $amo->addLead($lead);
      if (!($contact = $amo->checkContact($message))) {
        $phone_field_id = $this->form_config->get('phone');
        $email_field_id = $this->form_config->get('email');
        $name_field_id = $this->form_config->get('name');
        $custom_field = [];
        if ($message->hasField($phone_field_id)) {
          $custom_field['phone'] = [
            'id' => $this->form_config->get('phone-id'),
            'values' => [
              ['value' => $message->$phone_field_id->value],
            ],
          ];
        }
        if ($message->hasField('field_contact')) {
          $custom_field['phone'] = [
            'id' => $this->form_config->get('phone-id'),
            'values' => [
              ['value' => $message->field_contact->value],
            ],
          ];
        }
        if ($message->hasField($email_field_id)) {
          $custom_field['email'] = [
            'id' => $this->form_config->get('email-id'),
            'values' => [
              ['value' => $message->$email_field_id->value],
            ],
          ];
        }
        $contact = [
          'name' => $message->hasField($name_field_id) && !$message->$name_field_id->isEmpty() ? $message->$name_field_id->value : 'Посетитель сайта',
          'linked_leads_id' => $amo_lead,
          'custom_fields' => $custom_field,
        ];
        \Drupal::service('amo.api')
          ->addContact($contact, $amo_lead);
      }
      else {
        $link_update = $amo->updateLeadLinks($amo_lead, $contact);
      }
    }
    else {
      $contact_id = $amo->checkContact($message);
    }
  }

  /**
   *
   */
  public function checkContact($message, $client) {
    $phone_field_id = $this->form_config->get('phone');
    if ($message->hasField($phone_field_id)) {
      $phone_value = $message->$phone_field_id->value;
      $contacts = $client->contacts()->get()->all();
      foreach ($contacts as $contact) {
        $lead_custom_fields = $contact->getCustomFieldsValues();
        if ($lead_custom_fields) {
          $lead_phone_field = $lead_custom_fields->getBy('fieldCode', 'PHONE');
          if ($lead_phone_field) {
            $lead_phone_value = $lead_phone_field->getValues()->all()[0]->getValue();
            if ($lead_phone_value == $phone_value) {
              return FALSE;
            }
          }
        }
      }
    }
  }

  /**
   * Map.
   */
  private function mapCustomFields($custom_fields) {
    $fields = [];
    $map = [
      $this->form_config->get('content-id') => 'body',
    ];
    foreach ($map as $k => $v) {
      if ($k) {
        $values = [['value' => $custom_fields[$v]]];
        $fields[$v] = ['field_id' => $k, 'values' => $values];
      }
    }
    $fields = array_values($fields);
    return $fields;
  }

  /**
   * MessageToBody funciton.
   */
  private function messageToBody(object $message) : string {
    $fields = [];
    foreach ($message->getFieldDefinitions() as $key => $field) {
      if (substr($key, 0, 6) == 'field_') {
        $fields[$key] = $message->get($key)->value;
      }
    }
    $body = (!empty($fields)) ? implode("\n", $fields) : "";
    return $body;
  }

  /**
   * Push entity to queue.
   */
  public function createQueueItemCompleteOrderToAmo(OrderInterface $order) {
    $queue = \Drupal::queue('complete_order_to_amo');
    $queue->createQueue();
    // Добавляем элемент в очередь, которая выполняется по крону.
    $queue->createItem([
      'order_id' => $order->id(),
    ]);
  }

  /**
   * Process queue.
   */
  public function processQueueCompleteOrderToAmo() {
    $end = time() + 30;
    $queue = \Drupal::queue('complete_order_to_amo');
    if (!\Drupal::entityTypeManager()->hasDefinition('commerce_order')) {
      return FALSE;
    }
    $order_storage = \Drupal::entityTypeManager()
      ->getStorage('commerce_order');
    while (time() < $end && ($item = $queue->claimItem())) {
      $order = $order_storage->load($item->data['order_id']);
      $this->addCompleteOrderLead($order);
      $queue->deleteItem($item);
    }
  }

  /**
   * Push entity to queue.
   */
  public function queuePush(int $id) {
    $queue = \Drupal::queue('amo_lead');
    $queue->createQueue();
    // Добавляем элемент в очередь, которая выполняется по крону.
    $queue->createItem([
      'id' => $id,
    ]);
  }

  /**
   * Process queue.
   */
  public function queueProcess() {
    $end = time() + 30;
    $accountSwitcher = \Drupal::service('account_switcher');
    $accountSwitcher->switchTo(User::load(1));
    $queue = \Drupal::queue('amo_lead');
    while (time() < $end && ($item = $queue->claimItem())) {
      $this->init($item->data['id']);
      $queue->deleteItem($item);
    }
    $accountSwitcher->switchBack();
  }

}
