<?php

namespace Drupal\synamo\Service;

use AmoCRM\Client\AmoCRMApiClient;
use AmoCRM\Collections\CustomFieldsValuesCollection;
use AmoCRM\Collections\LinksCollection;
use AmoCRM\Collections\TagsCollection;
use AmoCRM\Exceptions\AmoCRMApiException;
use AmoCRM\Models\ContactModel;
use AmoCRM\Models\CustomFieldsValues\MultiselectCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\TextCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\MultiselectCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\TextCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueModels\MultiselectCustomFieldValueModel;
use AmoCRM\Models\CustomFieldsValues\ValueModels\TextCustomFieldValueModel;
use AmoCRM\Models\LeadModel;
use AmoCRM\Models\TagModel;
use AmoCRM\OAuth2\Client\Provider\AmoCRM;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Routing\TrustedRedirectResponse;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\HttpFoundation\Request;

/**
 * Docker Client service.
 */
class AmoApi {

  /**
   * Creates a new Docker Client.
   */
  public function __construct() {
    $config = \Drupal::config('synamo.settings');
    $this->token = '';
    $this->client = NULL;
    $this->clientId = $config->get('id');
    $this->clientSecret = $config->get('key');
    $this->authCode = $config->get('auth-code');
    $this->host = $config->get('amohost');
    if (!$this->host) {
      \Drupal::messenger()->addError('Адрес сервера не установлен');
      return FALSE;
    }
    $this->redirectUri = $config->get('ourhost') . '/amo';
    $this->config = \Drupal::service('config.factory')->getEditable('amo.token');
    $this->date = \Drupal::service('date.formatter');
    if ($this->config->get('tok')) {
      $this->getClientAccess();
      $tok = $this->getToken();
      $token = $tok->getToken();
      $this->opts = [
        'http_errors' => FALSE,
        'headers' => [
          'Accept-Encoding' => 'gzip',
          'Content-type' => "application/json",
          'Accept' => 'application/json',
          'Authorization' => "Bearer $token",
        ],
        'decode_content' => 'gzip',
        'connect_timeout' => 13.14,
        'query' => [],
      ];
    }
  }

  /**
   * Client Access.
   *
   * @return void
   */
  public function getClientAccess() {
    if ($this->clientId && $this->clientSecret && $this->redirectUri) {
      // code...
      $this->client = new AmoCRMApiClient($this->clientId, $this->clientSecret, $this->redirectUri);
      $this->token = $this->getToken();
      if ($this->token) {
        $this->client->setAccessToken($this->token)
          ->setAccountBaseDomain($this->host);
      }
    }
  }

  /**
   * Send Lead.
   */
  public function addLead($lead) {
    \Drupal::logger(__FUNCTION__ . __LINE__)->notice(
      '@j', ['@j' => json_encode($lead ?? [])]
    );
    $leadCustomFieldsValues = new CustomFieldsValuesCollection();
    foreach ($lead['custom_fields'] as $k => $value) {
      if (is_array($value['values'][0]['value'])) {
        if (!empty($value['values'][0]['value'])) {
          $multiselectCustomFieldValueModel = new MultiselectCustomFieldValuesModel();
          $multiselectCustomFieldValueModel->setFieldId($value['field_id']);
          $msColl = new MultiselectCustomFieldValueCollection();
          foreach ($value['values'][0]['value'] as $enum) {
            $enumId = new MultiselectCustomFieldValueModel();
            $enumId->setEnumId($enum);
            $msColl->add($enumId);
          }
          $multiselectCustomFieldValueModel->setValues($msColl);
          $leadCustomFieldsValues->add($multiselectCustomFieldValueModel);
        }
      }
      else {
        $textCustomFieldValueModel = new TextCustomFieldValuesModel();
        $textCustomFieldValueModel->setFieldId($value['field_id']);
        $textCustomFieldValueModel->setValues(
          (new TextCustomFieldValueCollection())
            ->add(
            (new TextCustomFieldValueModel())
              ->setValue($value['values'][0]['value']))
          );
        $leadCustomFieldsValues->add($textCustomFieldValueModel);
      }
    }
    $amolead = new LeadModel();
    $amolead->setName($lead['name']);
    $amolead->setPipelineId($lead['pipeline_id']);
    $amolead->setStatusId($lead['status_id']);
    if (isset($lead['tags'])) {
      $amolead->setTags(
        (new TagsCollection())
          ->add(
            (new TagModel())
              ->setName($lead['tags'])
          )
      );
    }
    $amolead->setCustomFieldsValues($leadCustomFieldsValues);
    try {
      $amolead = $this->client->leads()->addOne($amolead);
    }
    catch (AmoCRMApiException $e) {
      $amolead = FALSE;
    }

    return $amolead ?? FALSE;
  }

  /**
   * Send Contact.
   */
  public function addContact($contact, $lead) {
    if (!$lead) {
      return FALSE;
    }
    $contactCustomFieldsValues = new CustomFieldsValuesCollection();
    $textCustomFieldValueModel = new TextCustomFieldValuesModel();
    foreach ($contact['custom_fields'] as $key => $value) {
      $textCustomFieldValueModel->setFieldId($value['id']);
      $textCustomFieldValueModel->setValues(
        (new TextCustomFieldValueCollection())
          ->add((new TextCustomFieldValueModel())->setValue($value['values'][0]['value']))
      );
      $contactCustomFieldsValues->add($textCustomFieldValueModel);
    }

    $amocontact = new ContactModel();
    $amocontact->setName($contact['name'])
      ->setCustomFieldsValues($contactCustomFieldsValues);
    try {
      $amocontact = $this->client->contacts()->addOne($amocontact);
    }
    catch (AmoCRMApiException $e) {
      $amocontact = FALSE;
    }
    $links = new LinksCollection();
    $links->add($amocontact);
    try {
      $this->client->leads()->link($lead, $links);
    }
    catch (AmoCRMApiException $e) {
      $links = FALSE;
    }
    return $amocontact;
  }

  /**
   * Check Contact.
   */
  public function checkContact($message, $webform = FALSE, $phone = FALSE) {
    $phone_field = [
      'profile' => 'field_customer_phone',
      'contact_message' => 'field_phone',
    ];
    if ($message->hasField($phone_field[$message->getEntityType()->id()])) {
      $field_name = $phone_field[$message->getEntityType()->id()];
      $phone_value = $message->get($field_name)->value ?? '';
      $number = str_replace(['(', ')', '-', ' '], '', $phone_value);
      $number = urlencode($number);
      $link = 'https://' . $this->host . '/api/v4/contacts?query=' . $number;
      $headers = [
        'Content-Type: application/hal+json',
        'Authorization: Bearer ' . $this->token->getToken(),
      ];
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
      curl_setopt($curl, CURLOPT_HEADER, FALSE);
      curl_setopt($curl, CURLOPT_URL, $link);
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($curl, CURLOPT_HEADER, FALSE);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
      $out = curl_exec($curl);
      $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      $code = (int) $code;
      $errors = [
        301 => 'Moved permanently.',
        400 => 'Wrong structure of the array of transmitted data, or invalid identifiers of custom fields.',
        401 => 'Not Authorized. There is no account information on the server. You need to make a request to another server on the transmitted IP.',
        403 => 'The account is blocked, for repeatedly exceeding the number of requests per second.',
        404 => 'Not found.',
        500 => 'Internal server error.',
        502 => 'Bad gateway.',
        503 => 'Service unavailable.',
      ];
      if ($code < 200 || $code > 204) {
        die("Error $code. " . ($errors[$code] ?? 'Undefined error'));
      }
      \Drupal::logger(__FUNCTION__ . __LINE__)->notice(
        '@j', ['@j' => json_encode($out) ?? []]
      );
      if (!$out) {
        return FALSE;
      }
      $result = Json::decode($out);
      $contacts = $result['_embedded']['contacts'];
      \Drupal::logger(__FUNCTION__ . __LINE__)->notice(
        '@j', ['@j' => json_encode($contacts[0]['id']) ?? []]
      );
      $contact = $this->client->contacts()->getOne($contacts[0]['id']);
      return $contact;
    }
    return FALSE;
  }

  /**
   *
   */
  public function updateLeadLinks($lead, $contact) {
    $links = $this->client->leads()->getLinks($lead);
    $links->add($contact);
    try {
      $this->client->leads()->link($lead, $links);
    }
    catch (AmoCRMApiException $e) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Get.
   */
  public function getUsers() {
    $users = [];
    $query = [
      'limit' => 250,
    ];
    $data = $this->clientGet('/users', $query);
    if (!empty($data['_embedded']['users'])) {
      foreach ($data['_embedded']['users'] as $user) {
        $users[$user['id']] = $user['name'];
      }
    }
    return $users;
  }

  /**
   * Auth.
   */
  public function oAuth2(Request $request) {
    if ($this->clientId && $this->clientSecret && $this->redirectUri) {
      // code...
      $state = $this->getState();
      $client = new AmoCRMApiClient($this->clientId, $this->clientSecret, $this->redirectUri);
      $code = $request->query->get('code');
      if (empty($this->getUsers())) {
        if ($code) {
          $referer = $request->query->get('referer');
          if ($referer) {
            $data = $this->auth($referer, $code);
            $client->setAccessToken($data);
            $token = $data->getToken();
            $response = new TrustedRedirectResponse($this->redirectUri);
            $response->send();
          }
        }
        else {
          $link = $this->getLink($client);
          return $link;
        }
      }
      else {
        $token = $this->getToken();
        $client->setAccessToken($token);
      }
      return $client;
    }
    return FALSE;
  }

  /**
   * Auth.
   */
  public function refreshToken($auth_token = FALSE) {
    $users = $this->getUsers();
    $expires = $this->config->getRawData()['tok']['expires'] ?? 0;
    if (empty($users) || $expires < time() + 60 * 60 * 12) {
      $url = "https://$this->host/oauth2/access_token";
      if ($auth_token) {
        $options = [
          "client_id" => $this->clientId,
          "client_secret" => $this->clientSecret,
          "grant_type" => "authorization_code",
          "code" => $this->authCode,
          "redirect_uri" => $this->redirectUri,
        ];
      }
      else {
        $refresh_token = $this->config->getRawData()['tok']['refresh_token'];
        $options = [
          "client_id" => $this->clientId,
          "client_secret" => $this->clientSecret,
          "grant_type" => "refresh_token",
          "refresh_token" => $refresh_token,
          "redirect_uri" => $this->redirectUri,
        ];
      }
      try {
        $response = \Drupal::httpClient()->request('POST', $url, [
          'json' => $options,
        ]);
      }
      catch (\Exception $e) {
        $response = $e->getResponse();
        \Drupal::logger(__CLASS__)->notice($e->getMessage());
      }
      $code = $response->getStatusCode();
      $json = $response->getBody()->getContents();
      if ($code == 200) {
        $data = Json::decode($json);
        $upd_token = [
          'token_type' => $data['token_type'],
          'access_token' => $data['access_token'],
          'refresh_token' => $data['refresh_token'],
          'expires' => time() + $data['expires_in'],
        ];
        $this->config
          ->set('tok', $upd_token)
          ->save();
      }
    }
  }

  /**
   * Auth.
   */
  private function auth($referer, $code) {
    $client = new AmoCRM([
      'clientId' => $this->clientId,
      'clientSecret' => $this->clientSecret,
      'redirectUri' => $this->redirectUri,
    ]);
    $client->setBaseDomain($referer);
    $data = $client->getAccessToken('authorization_code', [
      'code' => $code,
    ]);
    if ($data) {
      $this->setToken($data->jsonSerialize());
    }
    return $data;
  }

  /**
   * Token get.
   */
  private function getToken() {
    $token = $this->config->get('tok');
    return $token ? new AccessToken($token) : NULL;
  }

  /**
   * Token set.
   */
  private function setToken($json) {
    $this->config->set('tok', $json);
    $this->config->save();
  }

  /**
   * GET oAuth Link.
   */
  private function getLink($client) {
    $oAuthClient = $client->getOAuthClient();
    $link = $oAuthClient->getAuthorizeUrl([
      'state' => $this->state,
      'mode' => 'post_message',
    ]);
    return $link;
  }

  /**
   * State.
   */
  private function getState() {
    if (empty($this->state)) {
      $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $length = 32;

      // Generates a random string of 32 characters.
      $this->state = substr(str_shuffle(str_repeat($chars, ceil($length / strlen($chars)))), 1, $length);
    }
    return $this->state;
  }

  /**
   * Client GET.
   */
  private function clientGet($uri, $query = [], $header = []) {
    if (!isset($this->opts)) {
      return FALSE;
    }
    $options = $this->opts;
    $options['query'] = $query;
    $client = new Client([
      'base_uri' => "https://{$this->host}/api/v4/",
      'timeout'  => 10.0,
      'verify' => FALSE,
    ]);
    try {
      $response = $client->get("https://$this->host/api/v4{$uri}", $options);
      $code = $response->getStatusCode();
      $body = $response->getBody()->getContents();
      if (in_array(substr($body, 0, 1), ['{', '['])) {
        $body = Json::decode($body);
      }
      if ($code == 200) {
        return $body;
      }
      else {
        \Drupal::logger(__FUNCTION__ . __LINE__)->notice(
          '@j', ['@j' => json_encode($body) ?? []]
        );
      }
      return [
        'header' => $response->getHeaders(),
        'code' => $code,
        'body' => $body,
        'uri' => $uri,
      ];
    }
    catch (RequestException $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }
    return "FALSE";
  }

}
