<?php

namespace Drupal\synamo\Form;

/**
 * @file
 * Contains Drupal\synamo\Form\Settings.
 */

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the Amo form controller.
 */
class Settings extends ConfigFormBase {

  /**
   * Build.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('synamo.settings');
    if (empty($config->get('ourhost'))) {
      $ourhost = \Drupal::request()->getSchemeAndHttpHost();
    }
    else {
      $ourhost = $config->get('ourhost');
    }
    $form['ourhost'] = [
      '#type' => 'textfield',
      '#size' => 150,
      '#title' => $this->t('Our website'),
      '#default_value' => $ourhost,
      '#required' => TRUE,
    ];
    $form['amohost'] = [
      '#type' => 'textfield',
      '#size' => 150,
      '#title' => $this->t('API AmoCRM website'),
      '#default_value' => $config->get('amohost'),
      '#required' => TRUE,
    ];
    $form['key'] = [
      '#type' => 'textfield',
      '#size' => 150,
      '#title' => $this->t('The secret key'),
      '#default_value' => $config->get('key'),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'textfield',
      '#size' => 150,
      '#title' => $this->t('Integration ID'),
      '#default_value' => $config->get('id'),
      '#required' => TRUE,
    ];

    $form['authcode'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Код авторизации'),
      '#default_value' => $config->get('auth-code'),
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'synamo_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['synamo.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('synamo.settings');
    $config
      ->set('ourhost', $form_state->getValue('ourhost'))
      ->set('amohost', $form_state->getValue('amohost'))
      ->set('key', $form_state->getValue('key'))
      ->set('id', $form_state->getValue('id'))
      ->set('auth-code', $form_state->getValue('authcode'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
