<?php

namespace Drupal\synamo\Form;

/**
 * @file
 * Contains Drupal\synamo\Form\Settings.
 */

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the Amo form controller.
 */
class LeadSettings extends ConfigFormBase {

  /**
   * Build.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('synamo.lead_settings');
    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('Lead settings'),
      '#open' => TRUE,
    ];
    $form['general']['pipeline'] = [
      '#title' => $this->t('Pipeline id'),
      '#default_value' => $config->get('pipeline-id'),
      '#maxlength' => 20,
      '#size' => 15,
      '#type' => 'textfield',
    ];
    $form['general']['status'] = [
      '#title' => $this->t('Status id'),
      '#default_value' => $config->get('status-id'),
      '#maxlength' => 20,
      '#size' => 15,
      '#type' => 'textfield',
    ];
    $form['contact'] = [
      '#type' => 'details',
      '#title' => $this->t('Contact data'),
      '#open' => TRUE,
    ];
    $form['contact']['name'] = [
      '#title' => $this->t('Name field id'),
      '#default_value' => $config->get('name'),
      '#maxlength' => 20,
      '#size' => 15,
      '#type' => 'textfield',
    ];
    $form['contact']['name-id'] = [
      '#title' => $this->t('Name amo_field id'),
      '#default_value' => $config->get('name-id'),
      '#maxlength' => 20,
      '#size' => 15,
      '#type' => 'textfield',
    ];
    $form['contact']['phone'] = [
      '#title' => $this->t('Phone field id'),
      '#default_value' => $config->get('phone'),
      '#maxlength' => 20,
      '#size' => 15,
      '#type' => 'textfield',
    ];
    $form['contact']['phone-id'] = [
      '#title' => $this->t('Phone amo_field id'),
      '#default_value' => $config->get('phone-id'),
      '#maxlength' => 20,
      '#size' => 15,
      '#type' => 'textfield',
    ];
    $form['contact']['email'] = [
      '#title' => $this->t('Email field id'),
      '#default_value' => $config->get('email'),
      '#maxlength' => 20,
      '#size' => 15,
      '#type' => 'textfield',
    ];
    $form['contact']['email-id'] = [
      '#title' => $this->t('Email amo_field id'),
      '#default_value' => $config->get('email-id'),
      '#maxlength' => 20,
      '#size' => 15,
      '#type' => 'textfield',
    ];
    $form['order'] = [
      '#type' => 'details',
      '#title' => $this->t('Order data'),
      '#open' => TRUE,
    ];
    $form['order']['content'] = [
      '#title' => $this->t('Content field id'),
      '#default_value' => $config->get('content'),
      '#maxlength' => 20,
      '#size' => 15,
      '#type' => 'textfield',
    ];
    $form['order']['content-id'] = [
      '#title' => $this->t('Content amo_field id'),
      '#default_value' => $config->get('content-id'),
      '#maxlength' => 20,
      '#size' => 15,
      '#type' => 'textfield',
    ];
    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'synamo_lead_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['synamo.lead_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('synamo.lead_settings');
    $config
      ->set('pipeline-id', $form_state->getValue('pipeline'))
      ->set('status-id', $form_state->getValue('status'))
      ->set('name', $form_state->getValue('name'))
      ->set('name-id', $form_state->getValue('name-id'))
      ->set('phone', $form_state->getValue('phone'))
      ->set('phone-id', $form_state->getValue('phone-id'))
      ->set('email', $form_state->getValue('email'))
      ->set('email-id', $form_state->getValue('email-id'))
      ->set('content', $form_state->getValue('content'))
      ->set('content-id', $form_state->getValue('content-id'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
